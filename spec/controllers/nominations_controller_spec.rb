require "spec_helper"
require "rails_helper"

describe NominationsController, type:  :controller  do
  describe "index" do
    it "renders the 'index' template - nomination" do
      get :index
      expect(response).to render_template("index")
    end
  end
    
    describe "new" do
    it "renders the 'new' template - create or modify nomination" do
      get :new
      expect(response).to render_template("nominations/new")
    end
  end


end
